﻿using System.Net;
using FluentAssertions;
using NUnit.Framework;

namespace Finance.Client.Api
{
	[TestFixture]
	public class ApiResponseStatusTests
	{
		public class IsSuccessStatusCodeTests
		{
			[TestCase(199, false)]
			[TestCase(200, true)]
			[TestCase(299, true)]
			[TestCase(300, false)]
			public void IsSuccessStatusCode(int statusCode, bool isSuccess)
			{
				//arrange
				var sut =new ApiResponseStatus { StatusCode = (HttpStatusCode)statusCode };

				//act
				var result = sut.IsSuccessStatusCode;

				//assert
				result.Should().Be(isSuccess);
			}
		}
	}
}
