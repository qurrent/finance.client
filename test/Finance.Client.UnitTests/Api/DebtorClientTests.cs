﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Finance.Client.Model;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Finance.Client.Api
{
	[TestFixture]
	public class DebtorClientTests
	{
		private static readonly Uri TestBaseUri = new Uri("http://0.0.0.0/v1/debtors/");

		public class ConstructorTests
		{
			[Test]
			public void When_initializing_without_financeClient_should_throw()
			{
				Action action = () => new DebtorsClient(null, new Mock<IHttpClient>().Object, TestBaseUri);

				action.ShouldThrow<ArgumentNullException>().And.ParamName.Should().Be("financeClient");
			}

			[Test]
			public void When_initializing_without_httpClient_should_throw()
			{
				Action action = () => new DebtorsClient(new Mock<IFinanceClient>().Object, null, TestBaseUri);

				action.ShouldThrow<ArgumentNullException>().And.ParamName.Should().Be("httpClient");
			}

			[Test]
			public void When_initializing_without_baseUri_should_throw()
			{
				Action action = () => new DebtorsClient(new Mock<IFinanceClient>().Object, new Mock<IHttpClient>().Object, null);

				action.ShouldThrow<ArgumentNullException>().And.ParamName.Should().Be("baseUri");
			}
		}

		public class ApiCallTests
		{
			private const int DebtorId = 123;
			private const string MandateReference = "2017123456781";
			private const string MandateReference2 = "2017123456782";

			private Mock<IHttpClient> _httpClientMock;

			private DebtorsClient _client;

			[SetUp]
			public void SetUp()
			{
				_httpClientMock = new Mock<IHttpClient>();

				_client = new DebtorsClient(new Mock<IFinanceClient>().Object, _httpClientMock.Object, TestBaseUri);
			}

			[Test]
			public async Task When_getting_debtor_by_id_succeeds_should_return_debtor()
			{
				var expectedResponse = new ApiResponse<GetDebtorResponse>
				{
					Data = new GetDebtorResponse
					{
						DebtorId = DebtorId,
						FirstName = "Voornaam",
						PhoneNumber = "0612345678"
					}
				};
				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.GetAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), "^http://0.0.0.0/v1/debtors/123$"))))
					.ReturnsAsync(mockedResponse)
					.Verifiable();

				// Act
				var response = await _client.GetByIdAsync(DebtorId);

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock.Verify();
			}

			[Test]
			public async Task When_creating_debtor_succeeds_should_return_debtor()
			{
				var expectedResponse = new ApiResponse<GetDebtorResponse>
				{
					Data = new GetDebtorResponse
					{
						DebtorId = DebtorId,
						FirstName = "Voornaam",
						PhoneNumber = "0612345678"
					}
				};
				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.PostAsync(It.IsAny<Uri>(), It.IsAny<StringContent>()))
					.ReturnsAsync(mockedResponse);

				// Act
				var response = await _client.CreateAsync(new CreateDebtorRequest
				{
					DebtorId = DebtorId,
					FirstName = "Voornaam",
					PhoneNumber = "0612345678"
				});

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock
					.Verify(m => m.PostAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), "^http://0.0.0.0/v1/debtors/$")), It.IsAny<StringContent>()), Times.Once);
			}

			[Test]
			public async Task When_updating_debtor_succeeds_should_return_debtor()
			{
				var expectedResponse = new ApiResponse<GetDebtorResponse>
				{
					Data = new GetDebtorResponse
					{
						DebtorId = DebtorId,
						FirstName = "Voornaam",
						PhoneNumber = "0612345678"
					}
				};
				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.PutAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), "^http://0.0.0.0/v1/debtors/123$")), It.IsAny<StringContent>()))
					.ReturnsAsync(mockedResponse)
					.Verifiable();

				// Act
				var response = await _client.UpdateAsync(new UpdateDebtorRequest
				{
					DebtorId = DebtorId,
					FirstName = "Voornaam",
					PhoneNumber = "0612345678"
				});

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock.Verify();
			}

			[Test]
			public async Task When_getting_mandate_by_reference_succeeds_should_return_mandate()
			{
				var expectedResponse = new ApiResponse<GetMandateResponse>
				{
					Data = new GetMandateResponse
					{
						DebtorId = DebtorId,
						BankAccountNumber = "banknr",
						IsActive = true,
						Reference = MandateReference,
						Type = MandateType.Recurrent
					}
				};
				var expectedUri = string.Format("^http://0.0.0.0/v1/debtors/{0}/mandates/{1}$", DebtorId, MandateReference);

				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.GetAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), expectedUri))))
					.ReturnsAsync(mockedResponse)
					.Verifiable();

				// Act
				var response = await _client.GetMandateByReferenceAsync(DebtorId, MandateReference);

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock.Verify();
			}

			[Test]
			public async Task When_getting_mandates_for_debtor_succeeds_should_return_collection_of_mandates()
			{
				var expectedResponse = new ApiResponse<IList<GetMandateResponse>>
				{
					Data = new List<GetMandateResponse>
					{
						new GetMandateResponse
						{
							DebtorId = DebtorId,
							BankAccountNumber = "banknr",
							IsActive = true,
							Reference = MandateReference,
							Type = MandateType.Recurrent
						},
						new GetMandateResponse
						{
							DebtorId = DebtorId,
							BankAccountNumber = "banknr",
							IsActive = false,
							Reference = MandateReference2,
							Type = MandateType.OneOff
						}
					}
				};
				var expectedUri = string.Format("^http://0.0.0.0/v1/debtors/{0}/mandates$", DebtorId);

				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.GetAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), expectedUri))))
					.ReturnsAsync(mockedResponse)
					.Verifiable();

				// Act
				var response = await _client.GetMandatesAsync(DebtorId);

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock.Verify();
			}

			[Test]
			public async Task When_creating_mandate_for_debtor_succeeds_should_return_debtor()
			{
				var expectedResponse = new ApiResponse<GetMandateResponse>
				{
					Data = new GetMandateResponse
					{
						DebtorId = DebtorId,
						BankAccountNumber = "banknr",
						IsActive = true,
						Reference = MandateReference,
						Type = MandateType.Recurrent
					}
				};
				var expectedUri = string.Format("^http://0.0.0.0/v1/debtors/{0}/mandates$", DebtorId);
				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.PostAsync(It.IsAny<Uri>(), It.IsAny<StringContent>()))
					.ReturnsAsync(mockedResponse);

				// Act
				var response = await _client.CreateMandateForDebtorAsync(new CreateMandateRequest
				{
					DebtorId = DebtorId,
					BankAccountNumber = "banknr",
					IsActive = true,
					Reference = MandateReference,
					Type = MandateType.Recurrent
				});

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock
					.Verify(m => m.PostAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), expectedUri)), It.IsAny<StringContent>()), Times.Once);
			}

			[Test]
			public async Task When_getting_invoices_for_debtor_succeeds_should_return_collection_of_invoices()
			{
				var expectedResponse = new ApiResponse<IList<GetInvoiceResponse>>
				{
					Data = new List<GetInvoiceResponse>
					{
						new GetInvoiceResponse
						{
							DebtorId = DebtorId,
							Date = new DateTimeOffset(new DateTime(2017,1,1)),
							Description = "Time to pay up, bromigo.",
							Details = new Dictionary<string, string>
							{
								{"documenttype","invoice"}
							},
							DocumentUri = new Uri("http://somewhereovertherainbow.com/321"),
							InvoiceId = new Guid(),
							ISOCurrencySymbol = "EUR",
							Number = "123456",
							PaymentStatus = new FinancialTransactionPaymentStatus(),
							TotalAmount = (decimal) 123.45
						},
						new GetInvoiceResponse
						{
							DebtorId = DebtorId,
							Date = new DateTimeOffset(new DateTime(2017,2,2)),
							Description = "Time to pay up again, bromigo.",
							Details = new Dictionary<string, string>
							{
								{"documenttype","vko"}
							},
							DocumentUri = new Uri("http://somewhereovertherainbow.com/123"),
							InvoiceId = new Guid(),
							ISOCurrencySymbol = "USD",
							Number = "654321",
							PaymentStatus = new FinancialTransactionPaymentStatus(),
							TotalAmount = (decimal) 543.21
						}
					}
				};
				var expectedUri = string.Format("^http://0.0.0.0/v1/debtors/{0}/invoices$", DebtorId);

				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.GetAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), expectedUri))))
					.ReturnsAsync(mockedResponse)
					.Verifiable();

				// Act
				var response = await _client.GetInvoicesAsync(DebtorId);

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock.Verify();
			}

			[Test]
			public async Task When_deleting_debtor_by_id_succeeds_should_return_debtor()
			{
				var expectedResponse = new ApiResponse<object>();
				var mockedResponse = GetMockedResponse(expectedResponse);

				_httpClientMock
					.Setup(m => m.DeleteAsync(It.Is<Uri>(s => Regex.IsMatch(s.ToString(), "^http://0.0.0.0/v1/debtors/123$"))))
					.ReturnsAsync(mockedResponse)
					.Verifiable();

				// Act
				var response = await _client.DeleteAsync(DebtorId);

				// Assert
				response.Should().NotBeNull().And.Subject.ShouldBeEquivalentTo(expectedResponse);
				_httpClientMock.Verify();
			}

			/// <summary>
			/// Turns the response into a JSON string, and wraps it in a <see cref="HttpResponseMessage"/>.
			/// </summary>
			/// <typeparam name="T"></typeparam>
			/// <param name="response"></param>
			/// <returns></returns>
			private static HttpResponseMessage GetMockedResponse<T>(ApiResponse<T> response)
			{
				if (response.Result == null)
				{
					response.Result = new ApiResponseStatus
					{
						StatusCode = HttpStatusCode.OK
					};
				}

				var json = JsonConvert.SerializeObject(response);

				var responseMessage = new HttpResponseMessage(HttpStatusCode.OK)
				{
					Content = new StringContent(json)
				};
				responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypes.Json);

				responseMessage.StatusCode = response.Result.StatusCode;
				return responseMessage;
			}

			public class When_calling_GetAsync : ApiCallTests
			{
				[Test]
				public async Task It_should_use_the_httpClient()
				{
					// Arrange
					var response = new HttpResponseMessage
					{
						StatusCode = HttpStatusCode.OK
					};

					_httpClientMock.Setup(m => m.GetAsync(It.IsAny<Uri>()))
						.ReturnsAsync(response)
						.Verifiable();

					// Act
					await _client.GetBalanceAsync(0, 0).ConfigureAwait(false);

					// Assert
					_httpClientMock.Verify();
				}

				[Test]
				public async Task It_should_return_balance_information()
				{
					// Arrange
					var debtorId = 123;
					var year = 1999;

					var expectedBalance = new BalanceResponse
					{
						Year = year, Balance = 1, DebtorId = debtorId
					};

					var expectedResult = new ApiResponse<BalanceResponse>
					{
						Data = expectedBalance
					};

					var mockedResponse = GetMockedResponse(expectedResult);

					_httpClientMock.Setup(m => m.GetAsync(new Uri(TestBaseUri, $"{debtorId}/balance/{year}")))
						.ReturnsAsync(mockedResponse)
						.Verifiable();

					// Act
					var result = await _client.GetBalanceAsync(debtorId, year).ConfigureAwait(false);

					// Assert
					_httpClientMock.Verify();
					result.Should().NotBeNull();
					result.Data.ShouldBeEquivalentTo(expectedBalance);
				}
			}
		}
	}
}