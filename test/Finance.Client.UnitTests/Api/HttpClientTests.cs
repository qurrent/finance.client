﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace Finance.Client.Api
{
	// WARN: dit is een kopie uit Qurrent.Core en moet uiteindelijk via Nuget worden gereferenced.
	[TestFixture]
	public class HttpClientTests
	{
		[Test]
		public async Task GetCallsRequest()
		{
			var uri = new Uri("http://google.com");
			var httpClientMock = new Mock<HttpClient> { CallBase = true };
			var expectedResponse = new Mock<HttpResponseMessage>().Object;
			var httpClient = httpClientMock.Object;

			httpClientMock
				.Setup(m => m.SendAsync(uri, HttpMethod.Get, null))
				.ReturnsAsync(expectedResponse)
				.Verifiable();

			// Act
			var response = await httpClient.GetAsync(uri);

			// Assert
			Assert.AreEqual(expectedResponse, response);
			httpClientMock.Verify();
		}


		[Test]
		public async Task PostCallsRequest()
		{
			var uri = new Uri("http://google.com");
			var httpClientMock = new Mock<HttpClient> { CallBase = true };
			var expectedResponse = new Mock<HttpResponseMessage>().Object;
			var httpClient = httpClientMock.Object;
			var data = new ByteArrayContent(new byte[] { 0xFF, 0xE0, 0x00, 0x11 });

			httpClientMock
				.Setup(m => m.SendAsync(uri, HttpMethod.Post, data))
				.ReturnsAsync(expectedResponse)
				.Verifiable();

			// Act
			var response = await httpClient.PostAsync(uri, data);

			// Assert
			Assert.AreEqual(expectedResponse, response);
			httpClientMock.Verify();
		}


		[Test]
		public async Task PutCallsRequest()
		{
			var uri = new Uri("http://google.com");
			var httpClientMock = new Mock<HttpClient> { CallBase = true };
			var expectedResponse = new Mock<HttpResponseMessage>().Object;
			var httpClient = httpClientMock.Object;
			var data = new ByteArrayContent(new byte[] { 0xFF, 0xE0, 0x00, 0x11 });

			httpClientMock
				.Setup(m => m.SendAsync(uri, HttpMethod.Put, data))
				.ReturnsAsync(expectedResponse)
				.Verifiable();

			// Act
			var response = await httpClient.PutAsync(uri, data);

			// Assert
			Assert.AreEqual(expectedResponse, response);
			httpClientMock.Verify();
		}


		[Test]
		public async Task DeleteCallsRequest()
		{
			var uri = new Uri("http://google.com");
			var httpClientMock = new Mock<HttpClient> { CallBase = true };
			var expectedResponse = new Mock<HttpResponseMessage>().Object;
			var httpClient = httpClientMock.Object;

			httpClientMock
				.Setup(m => m.SendAsync(uri, HttpMethod.Delete, null))
				.ReturnsAsync(expectedResponse)
				.Verifiable();

			// Act
			var response = await httpClient.DeleteAsync(uri);

			// Assert
			Assert.AreEqual(expectedResponse, response);
			httpClientMock.Verify();
		}
	}
}
