﻿using System;
using System.Net;
using System.Threading.Tasks;
using Finance.Client.Api;
using Finance.Client.Model;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace Finance.Client
{
	[TestFixture]
	public class FinanceClientTests
	{
		private static readonly Uri ApiBaseUri = new Uri("http://127.0.0.1");

		public class ConstructorTests
		{
			[Test]
			public void When_initializing_without_httpClient_should_throw()
			{
				Action action = () => new FinanceClient(null, ApiBaseUri);

				action.ShouldThrow<ArgumentNullException>().And.ParamName.Should().Be("httpClient");
			}

			[Test]
			public void When_initializing_without_uri_should_throw()
			{
				Action action = () => new FinanceClient(new Mock<IHttpClient>().Object, null);

				action.ShouldThrow<ArgumentNullException>().And.ParamName.Should().Be("apiBaseUri");
			}
		}

		[Test]
		public void When_accessing_debtors_should_return_instance()
		{
			var client = new FinanceClient(new Mock<IHttpClient>().Object, ApiBaseUri);

			client.Debtors.Should().NotBeNull().And.BeOfType<DebtorsClient>();
		}

		[Category("Integration")]
		public class IntegrationTests
		{
			private FinanceClient _financeClient;

			[SetUp]
			public void SetUp()
			{
				_financeClient = new FinanceClient(new HttpClient(), new Uri("http://finance-dev.qurrent.nl"));
			}

			[Test]
			public async Task When_getting_debtor_by_id_should_succeed()
			{
				var response = await _financeClient.Debtors.GetByIdAsync(1);

				Console.WriteLine(response);
				response.Result.StatusCode.Should().Be(HttpStatusCode.OK);
				response.Data.Should().NotBeNull().And.Subject.As<Model.GetDebtorResponse>().DebtorId.Should().Be(1);
			}

			[Test]
			public async Task When_getting_non_existing_debtor_by_id_should_return_404()
			{
				var response = await _financeClient.Debtors.GetByIdAsync(int.MaxValue);

				Console.WriteLine(response);
				response.Result.StatusCode.Should().Be(HttpStatusCode.NotFound);
				response.Data.Should().BeNull();
			}

			[Test]
			public async Task When_creating_debtor_with_invalid_model_should_return_400()
			{
				var response = await _financeClient.Debtors.CreateAsync(new CreateDebtorRequest
				{
					DebtorId = 123,
					FirstName = "Test"
				});

				Console.WriteLine(response);
				response.Result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
				response.Result.Message.Should().Contain("Het veld LastName is vereist.");
				response.Data.Should().BeNull();
			}

		}
	}
}
