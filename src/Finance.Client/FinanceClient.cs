﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Finance.Client.Api;
using Finance.Client.Model;

namespace Finance.Client
{
	/// <summary>
	/// A client implementation for accessing the finance micro service.
	/// </summary>
	public class FinanceClient
		: IFinanceClient
	{
		private readonly IHttpClient _httpClient;
		private readonly Uri _apiBaseUri;

		private IDebtorsClient _debtors;

		/// <summary>
		/// Initializes a new instance of <see cref="FinanceClient"/>.
		/// </summary>
		/// <param name="httpClient">The http client to use.</param>
		/// <param name="apiBaseUri">The base uri of the api (without version), f.ex. http://finance-dev.qurrent.nl </param>
		public FinanceClient(IHttpClient httpClient, Uri apiBaseUri)
		{
			if (httpClient == null)
				throw new ArgumentNullException("httpClient");
			if (apiBaseUri == null)
				throw new ArgumentNullException("apiBaseUri");

			_httpClient = httpClient;
			_apiBaseUri = apiBaseUri;			
		}

		/// <summary>
		/// Gets access to debtors.
		/// </summary>
		public IDebtorsClient Debtors
		{
			get { return _debtors ?? (_debtors = new DebtorsClient(this, _httpClient, new Uri(_apiBaseUri, "/v1/debtors/"))); }
		}
	}
}
