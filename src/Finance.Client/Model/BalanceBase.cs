﻿namespace Finance.Client.Model
{
	public class BalanceBase
	{
		public int Year { get; set; }

		public decimal Balance { get; set; }
	}
}