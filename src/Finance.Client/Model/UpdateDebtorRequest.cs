﻿using System.ComponentModel.DataAnnotations;

namespace Finance.Client.Model
{
	/// <summary>
	/// Represents a debtor update request.
	/// </summary>
	public class UpdateDebtorRequest : DebtorBase
	{
		/// <summary>
		/// Gets or sets the id of the associated debtor.
		/// </summary>
		public virtual int DebtorId { get; set; }

		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		[Required]
		public override string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		[Required]
		public override string LastName { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		[Required]
		public override string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the phone number.
		/// </summary>
		[Required]
		public override string PhoneNumber { get; set; }

		/// <summary>
		/// Gets or sets the payment method.
		/// </summary>
		[Required]
		public override PaymentMethod PaymentMethod { get; set; }

		/// <summary>
		/// Gets or sets the address.
		/// </summary>
		[Required]
		public override Address Address { get; set; }
	}
}
