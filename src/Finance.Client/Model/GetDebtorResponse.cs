﻿namespace Finance.Client.Model
{
	/// <summary>
	/// The response for a debtor.
	/// </summary>
	public class GetDebtorResponse : DebtorBase
	{
		/// <summary>
		/// Gets or sets the debtor id.
		/// </summary>
		public virtual int DebtorId { get; set; }
	}
}