﻿namespace Finance.Client.Model
{
	/// <summary>
	/// An address.
	/// </summary>
	public class Address
	{
		/// <summary>
		/// Gets or sets the street.
		/// </summary>
		public virtual string Street { get; set; }

		/// <summary>
		/// Gets or sets the housenumber.
		/// </summary>
		public virtual int HouseNumber { get; set; }

		/// <summary>
		/// Gets or sets the housenumber addition.
		/// </summary>
		public virtual string HouseNumberAddition { get; set; }

		/// <summary>
		/// Gets or sets the zip code.
		/// </summary>
		public virtual string ZipCode { get; set; }

		/// <summary>
		/// Gets or sets the city.
		/// </summary>
		public virtual string City { get; set; }
	}
}
