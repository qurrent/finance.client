﻿namespace Finance.Client.Model
{
	/// <summary>
	/// The sales order status
	/// </summary>
	public enum SalesOrderStatus
	{
		/// <summary>
		/// Ingevoerd
		/// </summary>
		Created = 21401,
		/// <summary>
		/// Bevestigd
		/// </summary>
		Confirmed = 21402,
		/// <summary>
		/// In backorder
		/// </summary>
		InBackOrder = 21403,
		/// <summary>
		/// In gebruik (wordt gemuteerd)
		/// </summary>
		InUse = 21404,
		/// <summary>
		/// Geplaatst
		/// </summary>
		Placed = 21405,
		/// <summary>
		/// Gefiatteerd
		/// </summary>
		Quoted = 2140,
		/// <summary>
		/// Afgehandeld
		/// </summary>
		Processed = 21407
	}
}
