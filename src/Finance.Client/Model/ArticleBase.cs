﻿namespace Finance.Client.Model
{
	/// <summary>
	/// Represents an article.
	/// </summary>
	public class ArticleBase
	{
		/// <summary>
		/// Get or sets the code.
		/// </summary>
		public virtual string Code { get; set; }

		/// <summary>
		/// Get or sets the description.
		/// </summary>
		public virtual string Description { get; set; }

		/// <summary>
		/// Get or sets the quantity.
		/// </summary>
		public virtual int Quantity { get; set; }
		
		/// <summary>
		/// Get or sets the price.
		/// </summary>
		public virtual decimal Price { get; set; }
	}
}
