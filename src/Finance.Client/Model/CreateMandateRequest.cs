﻿using System.ComponentModel.DataAnnotations;

namespace Finance.Client.Model
{
	/// <summary>
	/// Represents a mandate create request.
	/// </summary>
    public class CreateMandateRequest : MandateBase
    {
		/// <summary>
		/// Gets or sets the id of the associated debtor.
		/// </summary>
        [Required]
        public virtual int DebtorId { get; set; }

		/// <summary>
		/// Gets or sets the mandate description.
		/// </summary>
        [Required]
        public override string Description { get; set; }

		/// <summary>
		/// Gets or sets the bank account number.
		/// </summary>
        [Required]
        public override string BankAccountNumber { get; set; }

		/// <summary>
		/// Gets or sets whether the mandate is active. Inactive mandates should not be used.
		/// </summary>
        [Required]
        public override bool IsActive { get; set; }

		/// <summary>
		/// Gets or sets the mandate type.
		/// </summary>
        [Required]
        public override MandateType Type { get; set; }
    }
}