﻿namespace Finance.Client.Model
{
    /// <summary>
    /// The response for a mandate.
    /// </summary>
    public class GetMandateResponse : MandateBase
    {
		/// <summary>
		/// Gets or sets the id of the associated debtor.
		/// </summary>
		public virtual int DebtorId { get; set; }
    }
}