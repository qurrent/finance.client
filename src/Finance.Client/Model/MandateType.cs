﻿namespace Finance.Client.Model
{
	/// <summary>
	/// The mandate type.
	/// </summary>
    public enum MandateType
    {
		/// <summary>
		/// Recurrent mandate.
		/// </summary>
        Recurrent,
		/// <summary>
		/// One off mandate.
		/// </summary>
        OneOff
    }
}