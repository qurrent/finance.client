﻿using System.Collections.Generic;

namespace Finance.Client.Model
{
	/// <summary>
	/// Represents the debtor base.
	/// </summary>
	public class DebtorBase
	{
		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		public virtual string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the preposition.
		/// </summary>
		public virtual string Preposition { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		public virtual string LastName { get; set; }

		/// <summary>
		/// Gets or sets the phone number.
		/// </summary>
		public virtual string PhoneNumber { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		public virtual string EmailAddress { get; set; }

		/// <summary>
		/// Gets or sets the payment method.
		/// </summary>
		public virtual PaymentMethod PaymentMethod { get; set; }

		/// <summary>
		/// Gets or sets the address.
		/// </summary>
		public virtual Address Address { get; set; }

		/// <summary>
		/// Gets or sets a collection of bank accounts.
		/// </summary>
		public virtual ICollection<BankAccount> BankAccounts { get; set; }
	}
}