﻿namespace Finance.Client.Model
{
	public class BalanceResponse : BalanceBase
	{
		/// <summary>
		/// Gets or sets the debtor id.
		/// </summary>
		public virtual int DebtorId { get; set; }
	}
}