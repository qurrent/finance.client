﻿namespace Finance.Client.Model
{
	/// <summary>
	/// Represents the mandate base.
	/// </summary>
    public class MandateBase
    {
        /// <summary>
        ///     Gets or sets the reference.
        /// </summary>
        public virtual string Reference { get; set; }

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        ///     Gets or sets the Bank account number.
        /// </summary>
        public virtual string BankAccountNumber { get; set; }

        /// <summary>
        ///     Gets or sets the Active.
        /// </summary>
        public virtual bool IsActive { get; set; }

        /// <summary>
        ///     Gets or sets the mandate type.
        /// </summary>
        public virtual MandateType Type { get; set; }
    }
}