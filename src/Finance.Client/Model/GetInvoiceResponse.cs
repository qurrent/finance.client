﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using Qurrent.Json.Newtonsoft.Converters;

namespace Finance.Client.Model
{
	/// <summary>
	/// The response for an invoice.
	/// </summary>
	[SuppressMessage("ReSharper", "UnusedMember.Global")]
	public class GetInvoiceResponse
	{
		/// <summary>
		/// Gets or sets the debtor id.
		/// </summary>
		public int DebtorId { get; set; }

		/// <summary>
		/// Gets or sets the invoice type.
		/// </summary>
		public string Type { get; set; }

		/// <summary>
		/// Gets or sets the invoice number.
		/// </summary>
		public string Number { get; set; }

		/// <summary>
		/// Gets or sets the invoice date (boekdatum / factuurdatum).
		/// </summary>
		[JsonConverter(typeof(IsoDateConverter))]
		public DateTimeOffset Date { get; set; }

		/// <summary>
		/// Gets or sets the description of the transaction.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the amount.
		/// </summary>
		public decimal TotalAmount { get; set; }

		/// <summary>
		/// Gets or sets the ISO currency symbol.
		/// </summary>
		[SuppressMessage("ReSharper", "InconsistentNaming")]
		public string ISOCurrencySymbol { get; set; }

		/// <summary>
		/// Gets or sets the payment status.
		/// </summary>
		public FinancialTransactionPaymentStatus PaymentStatus { get; set; } = new FinancialTransactionPaymentStatus();

		/// <summary>
		/// Gets or sets the (optional) invoice id.
		/// </summary>
		public Guid? InvoiceId { get; set; }

		/// <summary>
		/// Gets or sets extra invoice details.
		/// </summary>
		public IDictionary<string, string> Details { get; set; }


		/// <summary>
		/// Gets or sets the uri where the PDF document can be downloaded.
		/// </summary>
		public Uri DocumentUri { get; set; }
	}
}