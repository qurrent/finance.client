﻿using System;
using System.Collections.Generic;

namespace Finance.Client.Model
{
	/// <summary>
	/// Represents a sales order.
	/// </summary>
	public class SalesOrderBase
	{
		/// <summary>
		/// Get or sets the id.
		/// </summary>
		public virtual long OrderId { get; set; }

		/// <summary>
		/// Get or sets the iDEAL id.
		/// </summary>
		public virtual string IdealId { get; set; }

		/// <summary>
		/// Get or sets the debtor.
		/// </summary>
		public virtual int DebtorId { get; set; }

		/// <summary>
		/// Get or sets the order date.
		/// </summary>
		public virtual DateTime OrderDate { get; set; }

		/// <summary>
		/// Get or sets the delivery date.
		/// </summary>
		public virtual DateTime? DeliveryDate { get; set; }

		/// <summary>
		/// Get or sets the payment method.
		/// </summary>
		public virtual PaymentMethod PaymentMethod { get; set; }

		/// <summary>
		/// Get or sets the articles.
		/// </summary>
		public virtual ICollection<ArticleBase> Articles { get; set; }

		/// <summary>
		/// Get or sets the sales order status.
		/// </summary>
		public SalesOrderStatus OrderStatus { get; set; }

		/// <summary>
		/// Get or sets the mandate reference
		/// </summary>
		public virtual string MandateReference { get; set; }
	}
}
