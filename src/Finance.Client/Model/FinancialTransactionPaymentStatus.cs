﻿using System;

namespace Finance.Client.Model
{
	/// <summary>
	/// Represents the payment status of an invoice.
	/// </summary>
	public class FinancialTransactionPaymentStatus
	{
		/// <summary>
		/// Gets whether the <see cref="Status"/> is <see cref="PaymentStatus.Paid"/>.
		/// </summary>
		public bool IsPaid { get; set; }

		/// <summary>
		/// Gets or sets the payment status.
		/// </summary>
		public PaymentStatus Status { get; set; }

		/// <summary>
		/// Gets or sets the payment date. Is null when not payed.
		/// </summary>
		public DateTimeOffset? Date { get; set; }
	}
}