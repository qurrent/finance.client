﻿using System.ComponentModel.DataAnnotations;

namespace Finance.Client.Model
{
	/// <summary>
	/// Represents an article.
	/// </summary>
	public class CreateArticleRequest : ArticleBase
	{
		/// <summary>
		/// Get or sets the code.
		/// </summary>
		[Required]
		public override string Code { get; set; }

		/// <summary>
		/// Get or sets the description.
		/// </summary>
		[Required]
		public override string Description { get; set; }

		/// <summary>
		/// Get or sets the quantity.
		/// </summary>
		[Required]
		public override int Quantity { get; set; }

		/// <summary>
		/// Get or sets the price.
		/// </summary>
		[Required]
		public override decimal Price { get; set; }
	}
}
