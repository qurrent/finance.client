﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Finance.Client.Model
{
	/// <summary>
	/// Represents a sales order.
	/// </summary>
	public class CreateSalesOrderRequest : SalesOrderBase
	{
		/// <summary>
		/// Get or sets the id.
		/// </summary>
		[Required]
		public override long OrderId { get; set; }

		/// <summary>
		/// Get or sets the debtor.
		/// </summary>
		[Required]
		public override int DebtorId { get; set; }

		/// <summary>
		/// Get or sets the order date.
		/// </summary>
		[Required]
		public override DateTime OrderDate { get; set; }

		/// <summary>
		/// Get or sets the payment method.
		/// </summary>
		[Required]
		public override PaymentMethod PaymentMethod { get; set; }

		/// <summary>
		/// Get or sets the articles.
		/// </summary>
		[Required]
		public override ICollection<ArticleBase> Articles { get; set; }
	}
}