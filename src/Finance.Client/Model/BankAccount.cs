﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Finance.Client.Model
{
	/// <summary>
	/// A bank account.
	/// </summary>
	public class BankAccount
	{
		/// <summary>
		/// Initializes a new instance of <see cref="BankAccount"/>.
		/// </summary>
		public BankAccount()
		{
			Default = true;
		}

		/// <summary>
		/// The bank account number.
		/// </summary>
		[Required]
		public string AccountNumber { get; set; }

		/// <summary>
		/// The name of the owner of the bank account.
		/// </summary>
		[Required]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets if the bank account is the default account (primary).
		/// </summary>
		[DefaultValue(true)]
		[JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
		public bool Default { get; set; }
	}
}
