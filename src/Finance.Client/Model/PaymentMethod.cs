﻿namespace Finance.Client.Model
{
	/// <summary>
	///  The payment method.
	/// </summary>
	public enum PaymentMethod
	{
		/// <summary>
		/// Incasso
		/// </summary>
		Debit = 0,
		/// <summary>
		/// Bank/factuur
		/// </summary>
		Invoice = 1
	}
}
