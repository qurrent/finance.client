﻿namespace Finance.Client.Model
{
	/// <summary>
	/// Possible payment statuses of any kind of financial transaction.
	/// </summary>
	public enum PaymentStatus
	{
		/// <summary>
		/// Unknown, or not yet processed.
		/// </summary>
		None,

		/// <summary>
		/// Payment is being processed (f.ex. direct debit).
		/// </summary>
		InProcess,

		/// <summary>
		/// Paid (betaald).
		/// </summary>
		Paid
	}
}