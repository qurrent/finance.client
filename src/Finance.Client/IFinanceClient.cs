﻿using Finance.Client.Api;

namespace Finance.Client
{
	/// <summary>
	/// Represents a client implementation for accessing the finance micro service.
	/// </summary>
	public interface IFinanceClient
	{
		/// <summary>
		/// Gets access to debtors.
		/// </summary>
		IDebtorsClient Debtors { get; }
	}
}
