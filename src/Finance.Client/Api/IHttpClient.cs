﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Finance.Client.Api
{
	/// <summary>
	/// Describes the interface for a HTTP client.
	/// </summary>
	public interface IHttpClient
	{
		/// <summary>
		/// Performs a GET request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		Task<HttpResponseMessage> GetAsync(Uri uri);

		/// <summary>
		/// Performs a POST request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <param name="data">The data to post.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		Task<HttpResponseMessage> PostAsync(Uri uri, HttpContent data);

		/// <summary>
		/// Performs a PUT request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <param name="data">The data to put.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		Task<HttpResponseMessage> PutAsync(Uri uri, HttpContent data);

		/// <summary>
		/// Performs a DELETE request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		Task<HttpResponseMessage> DeleteAsync(Uri uri);

		/// <summary>
		/// Performs a request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <param name="method">The http method.</param>
		/// <param name="content">The (optional) content to send.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		Task<HttpResponseMessage> SendAsync(Uri uri, HttpMethod method, HttpContent content = null);
	}
}
