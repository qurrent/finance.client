﻿using System.Net;

namespace Finance.Client.Api
{
	/// <summary>
	/// The response status meta data for an API request.
	/// </summary>
	public class ApiResponseStatus
	{
		/// <summary>
		/// Gets or sets the api version.
		/// </summary>
		public string Version { get; set; }

		/// <summary>
		/// Gets the status in string format.
		/// </summary>
		public string Status { get; set; }

		/// <summary>
		/// Gets or sets the status code.
		/// </summary>
		public HttpStatusCode StatusCode { get; set; }

		/// <summary>
		/// Gets a value that indicates if the HTTP response was successful (&gt;= 200 and &lt;= 299).
		/// </summary>
		public bool IsSuccessStatusCode
		{
			get
			{
				return (int)StatusCode >= 200 && (int)StatusCode < 300;
			}
		}

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		public virtual string Message { get; set; }

		/// <summary>
		/// Gets or sets the error code.
		/// </summary>
		public virtual int? ErrorCode { get; set; }

		/// <summary>
		/// Gets or sets more info.
		/// </summary>
		public virtual string MoreInfo { get; set; }

		/// <summary>
		/// Gets or sets the exception type name.
		/// </summary>
		public virtual string Exception { get; set; }

		/// <summary>
		/// Gets or sets the source.
		/// </summary>
		public virtual string Source { get; set; }

		/// <summary>
		/// Gets or sets the stack trace.
		/// </summary>
		public virtual string StackTrace { get; set; }
	}
}