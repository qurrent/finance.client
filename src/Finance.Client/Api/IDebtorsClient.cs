﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Finance.Client.Model;

namespace Finance.Client.Api
{
	/// <summary>
	/// Represents a client for accessing the debtors micro service.
	/// </summary>
	public interface IDebtorsClient
	{
		/// <summary>
		/// Gets a debtor by id.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the debtor</returns>
		Task<ApiResponse<GetDebtorResponse>> GetByIdAsync(int debtorId);

		/// <summary>
		/// Gets a debtor by id.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the debtor</returns>
		ApiResponse<GetDebtorResponse> GetById(int debtorId);

		/// <summary>
		/// Creates a new debtor. If the debtor already exists, the call fails.
		/// </summary>
		/// <param name="newDebtor">The new debtor.</param>
		/// <returns>an api response containing the newly created debtor</returns>
		Task<ApiResponse<GetDebtorResponse>> CreateAsync(CreateDebtorRequest newDebtor);

		/// <summary>
		/// Creates a new debtor. If the debtor already exists, the call fails.
		/// </summary>
		/// <param name="newDebtor">The new debtor.</param>
		/// <returns>an api response containing the newly created debtor</returns>
		ApiResponse<GetDebtorResponse> Create(CreateDebtorRequest newDebtor);

		/// <summary>
		/// Updates a debtor.
		/// </summary>
		/// <param name="updatedDebtor">The debtor to update.</param>
		/// <returns>an api response containing the updated debtor</returns>
		Task<ApiResponse<GetDebtorResponse>> UpdateAsync(UpdateDebtorRequest updatedDebtor);

		/// <summary>
		/// Updates a debtor.
		/// </summary>
		/// <param name="updatedDebtor">The debtor to update.</param>
		/// <returns>an api response containing the updated debtor</returns>
		ApiResponse<GetDebtorResponse> Update(UpdateDebtorRequest updatedDebtor);

		/// <summary>
		/// Gets a mandate for a debtor by reference.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <param name="reference">The mandate reference.</param>
		/// <returns>an api response containing the mandate</returns>
		Task<ApiResponse<GetMandateResponse>> GetMandateByReferenceAsync(int debtorId, string reference);

		/// <summary>
		/// Gets a mandate for a debtor by reference.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <param name="reference">The mandate reference.</param>
		/// <returns>an api response containing the mandate</returns>
		ApiResponse<GetMandateResponse> GetMandateByReference(int debtorId, string reference);

		/// <summary>
		/// Gets a list of mandate for a debtor.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the list of mandates</returns>
		Task<ApiResponse<IList<GetMandateResponse>>> GetMandatesAsync(int debtorId);

		/// <summary>
		/// Gets a list of mandate for a debtor.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the list of mandates</returns>
		ApiResponse<IList<GetMandateResponse>> GetMandates(int debtorId);

		/// <summary>
		/// Creates a new mandate.
		/// </summary>
		/// <param name="newMandate">The new mandate.</param>
		/// <returns>an api response containing the newly created mandate</returns>
		Task<ApiResponse<GetMandateResponse>> CreateMandateForDebtorAsync(CreateMandateRequest newMandate);

		/// <summary>
		/// Creates a new mandate.
		/// </summary>
		/// <param name="newMandate">The new mandate.</param>
		/// <returns>an api response containing the newly created mandate</returns>
		ApiResponse<GetMandateResponse> CreateMandateForDebtor(CreateMandateRequest newMandate);

		/// <summary>
		/// Gets data on invoices asynchronously.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>An api response containing the list of invoices.</returns>
		Task<ApiResponse<IList<GetInvoiceResponse>>> GetInvoicesAsync(int debtorId);


		/// <summary>
		/// Gets data on invoices.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>An api response containing the list of invoices.</returns>
		ApiResponse<IList<GetInvoiceResponse>> GetInvoices(int debtorId);

		/// <summary>
		/// Gets the balance by debtor Id and year.
		/// </summary>
		/// <param name="debtorId">The debtor Id.</param>
		/// <param name="year">The year of the balance.</param>
		/// <returns>The balance of a debtor in a specific year.</returns>
		Task<ApiResponse<BalanceResponse>> GetBalanceAsync(int debtorId, int year);
	}
}