﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Finance.Client.Api
{
	/// <summary>
	/// Abstract base class for accessing an api end point.
	/// </summary>
	public abstract class JsonApiClient
	{
		/// <summary>
		/// Processes the response content, and deserializes it to type <typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="response"></param>
		/// <returns></returns>
		protected async Task<ApiResponse<T>> ProcessResponseAsync<T>(HttpResponseMessage response)
		{
			if (response.Content == null || ((response.Content.Headers.ContentLength ?? 0) <= 0))
			{
				return

					new ApiResponse<T>
					{
						Result = new ApiResponseStatus
						{
							Status = response.StatusCode.ToString().ToUpperInvariant(),
							StatusCode = response.StatusCode,
							Message = response.ReasonPhrase
						}
					};
			}

			switch (response.Content.Headers.ContentType.MediaType)
			{
				case MediaTypes.Json:
					var json = await response.Content.ReadAsStringAsync();
					return JsonConvert.DeserializeObject<ApiResponse<T>>(json);

				case MediaTypes.Html:
					return new ApiResponse<T>
					{
						Result = new ApiResponseStatus
						{
							Status = response.StatusCode.ToString().ToUpperInvariant(),
							StatusCode = response.StatusCode,
							Message = !response.IsSuccessStatusCode && response.Content != null
								? await response.Content.ReadAsStringAsync()
								: null
						}
					};

			}

			return new ApiResponse<T>
			{
				Result = new ApiResponseStatus
				{
					Status = response.StatusCode.ToString().ToUpperInvariant(),
					StatusCode = response.StatusCode,
					Message = response.ReasonPhrase
				}
			};
		}

		/// <summary>
		/// Processes the response content, and deserializes it to type <typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="response"></param>
		/// <returns></returns>
		protected ApiResponse<T> ProcessResponse<T>(HttpResponseMessage response)
		{
			if (response.Content == null || ((response.Content.Headers.ContentLength ?? 0) <= 0))
			{
				return

					new ApiResponse<T>
					{
						Result = new ApiResponseStatus
						{
							Status = response.StatusCode.ToString().ToUpperInvariant(),
							StatusCode = response.StatusCode,
							Message = response.ReasonPhrase
						}
					};
			}

			switch (response.Content.Headers.ContentType.MediaType)
			{
				case MediaTypes.Json:
					var json = response.Content.ReadAsStringAsync().Result;
					return JsonConvert.DeserializeObject<ApiResponse<T>>(json);

				case MediaTypes.Html:
					return new ApiResponse<T>
					{
						Result = new ApiResponseStatus
						{
							Status = response.StatusCode.ToString().ToUpperInvariant(),
							StatusCode = response.StatusCode,
							Message = !response.IsSuccessStatusCode && response.Content != null
								? response.Content.ReadAsStringAsync().Result
								: null
						}
					};

			}

			return new ApiResponse<T>
			{
				Result = new ApiResponseStatus
				{
					Status = response.StatusCode.ToString().ToUpperInvariant(),
					StatusCode = response.StatusCode,
					Message = response.ReasonPhrase
				}
			};
		}

		/// <summary>
		/// Creates a <see cref="HttpContent"/> with json data and headers.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>
		protected HttpContent CreateJsonContent<T>(T data)
		{
			var content = new StringContent(JsonConvert.SerializeObject(data));
			content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypes.Json);
			return content;
		}
	}
}