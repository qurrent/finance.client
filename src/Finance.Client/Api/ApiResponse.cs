﻿using System.Text;

namespace Finance.Client.Api
{
	/// <summary>
	/// Represents the full API response, including (error) result info and response data (if any).
	/// </summary>
	/// <typeparam name="T">The type of the response data.</typeparam>
	public class ApiResponse<T>
	{
		/// <summary>
		/// Gets or sets the response status.
		/// </summary>
		public ApiResponseStatus Result { get; set; }
		
//		/// <summary>
//		/// Gets or sets meta data.
//		/// </summary>
//		public object Meta { get; set; }
		
		/// <summary>
		/// Gets or sets the actual data/content.
		/// </summary>
		public T Data { get; set; }

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		/// <filterpriority>2</filterpriority>
		public override string ToString()
		{
			var sb = new StringBuilder();

			sb.AppendFormat("StatusCode: {0}", (int)Result.StatusCode);
			sb.AppendFormat(", ReasonPhrase: '{0}'", Result.Message ?? "<null>");
			if (Result.Version != null)
			{
				sb.AppendFormat(", Version: {0}", Result.Version);
			}
			sb.AppendFormat(", Content: {0}", Data == null ? "<null>" : Data.GetType().FullName);

			return sb.ToString();
		}
	}
}