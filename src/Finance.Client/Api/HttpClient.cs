﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Qurrent.Diagnostics.Logging;

namespace Finance.Client.Api
{
	// NOTE: this class is taken from Qurrent.Core and slighly modified. Unit tests are not carried over.

	/// <summary>
	/// Injectable helper for making HTTP calls. Can make easy GET, POST, PUT and DELETE calls including parameters. Results are returned as <see cref="HttpResponseMessage"/> objects.
	/// </summary>
	public class HttpClient : IHttpClient
	{
		private readonly System.Net.Http.HttpClient _client;

		private static readonly ILog Log = LogProvider.GetCurrentClassLogger();

		/// <summary>
		/// Initializes a new instance of <see cref="HttpClient"/>.
		/// </summary>
		public HttpClient()
		{
			var clientHandler = new HttpClientHandler
			{
				AllowAutoRedirect = false,
				AutomaticDecompression = DecompressionMethods.GZip
			};
			_client = new System.Net.Http.HttpClient(clientHandler)
			{
				Timeout = new TimeSpan(0, 0, 2, 0)
			};

			_client.DefaultRequestHeaders.Accept.Clear();
			_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypes.Json));
		}

		/// <summary>
		/// Gets or sets the time in milliseconds before a request times out.
		/// </summary>
		public TimeSpan Timeout
		{
			get { return _client.Timeout; }
			set { _client.Timeout = value; }
		}


		/// <summary>
		/// Performs a GET request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		public virtual Task<HttpResponseMessage> GetAsync(Uri uri)
		{
			return SendAsync(uri, HttpMethod.Get);
		}


		/// <summary>
		/// Performs a POST request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <param name="data">The data to post.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		public virtual Task<HttpResponseMessage> PostAsync(Uri uri, HttpContent data)
		{
			return SendAsync(uri, HttpMethod.Post, data);
		}


		/// <summary>
		/// Performs a PUT request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <param name="data">The data to put.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		public virtual Task<HttpResponseMessage> PutAsync(Uri uri, HttpContent data)
		{
			return SendAsync(uri, HttpMethod.Put, data);
		}


		/// <summary>
		/// Performs a DELETE request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		public virtual Task<HttpResponseMessage> DeleteAsync(Uri uri)
		{
			return SendAsync(uri, HttpMethod.Delete);
		}


		/// <summary>
		/// Performs a web request on specified <paramref name="uri"/>.
		/// </summary>
		/// <param name="uri">The request uri.</param>
		/// <param name="method">The http method.</param>
		/// <param name="content">The optional content to post.</param>
		/// <returns>Returns a <see cref="WebResponse"/> with the response of the request.</returns>
		public virtual Task<HttpResponseMessage> SendAsync(Uri uri, HttpMethod method, HttpContent content = null)
		{
			Log.Info("HTTP {0} request to: {1}", method, uri);

			var request = new HttpRequestMessage(method, uri)
			{
				Content = content
			};

			return _client.SendAsync(request);
		}
	}
}
