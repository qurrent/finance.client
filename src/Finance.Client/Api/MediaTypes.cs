﻿namespace Finance.Client.Api
{
	/// <summary>
	/// Media type constants
	/// </summary>
	internal static class MediaTypes
	{
		public const string Json = "application/json";
		public const string Html = "text/html";
	}
}