﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Finance.Client.Model;

namespace Finance.Client.Api
{
	/// <summary>
	/// A client for accessing the debtors micro service.
	/// </summary>
	public class DebtorsClient : JsonApiClient, IDebtorsClient
	{
		private readonly IFinanceClient _financeClient;
		private readonly IHttpClient _httpClient;
		private readonly Uri _baseUri;

		/// <summary>
		/// Initializes a new instance of <see cref="DebtorsClient"/>.
		/// </summary>
		/// <param name="financeClient">The parent finance client.</param>
		/// <param name="httpClient">The http client.</param>
		/// <param name="baseUri">The base uri of the endpoint (without controller).</param>
		internal DebtorsClient(IFinanceClient financeClient, IHttpClient httpClient, Uri baseUri)
		{
			if (financeClient == null)
				throw new ArgumentNullException("financeClient");
			if (httpClient == null)
				throw new ArgumentNullException("httpClient");
			if (baseUri == null)
				throw new ArgumentNullException("baseUri");

			_financeClient = financeClient;
			_httpClient = httpClient;
			_baseUri = baseUri;
		}

		/// <summary>
		/// Gets a debtor by id.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public async Task<ApiResponse<GetDebtorResponse>> GetByIdAsync(int debtorId)
		{
			var response = await _httpClient.GetAsync(new Uri(_baseUri, debtorId.ToString()));

			return await ProcessResponseAsync<GetDebtorResponse>(response);
		}

		/// <summary>
		/// Gets a debtor by id.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public ApiResponse<GetDebtorResponse> GetById(int debtorId)
		{
			var response = _httpClient.GetAsync(new Uri(_baseUri, debtorId.ToString())).GetAwaiter().GetResult();

			return ProcessResponse<GetDebtorResponse>(response);
		}

		/// <summary>
		/// Creates a new debtor. If the debtor already exists, the call fails.
		/// </summary>
		/// <param name="newDebtor">The new debtor.</param>
		/// <returns>an api response containing the newly created debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public async Task<ApiResponse<GetDebtorResponse>> CreateAsync(CreateDebtorRequest newDebtor)
		{
			var jsonData = CreateJsonContent(newDebtor);

			var response = await _httpClient.PostAsync(_baseUri, jsonData);

			return await ProcessResponseAsync<GetDebtorResponse>(response);
		}

		/// <summary>
		/// Creates a new debtor. If the debtor already exists, the call fails.
		/// </summary>
		/// <param name="newDebtor">The new debtor.</param>
		/// <returns>an api response containing the newly created debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public ApiResponse<GetDebtorResponse> Create(CreateDebtorRequest newDebtor)
		{
			var jsonData = CreateJsonContent(newDebtor);

			var response = _httpClient.PostAsync(_baseUri, jsonData).GetAwaiter().GetResult();

			return ProcessResponse<GetDebtorResponse>(response);
		}

		/// <summary>
		/// Updates a debtor.
		/// </summary>
		/// <param name="updatedDebtor">The debtor to update.</param>
		/// <returns>an api response containing the updated debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public async Task<ApiResponse<GetDebtorResponse>> UpdateAsync(UpdateDebtorRequest updatedDebtor)
		{
			var jsonData = CreateJsonContent(updatedDebtor);

			var response = await _httpClient.PutAsync(new Uri(_baseUri, updatedDebtor.DebtorId.ToString()), jsonData);

			return await ProcessResponseAsync<GetDebtorResponse>(response);
		}
		 
		/// <summary>
		/// Updates a debtor.
		/// </summary>
		/// <param name="updatedDebtor">The debtor to update.</param>
		/// <returns>an api response containing the updated debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public ApiResponse<GetDebtorResponse> Update(UpdateDebtorRequest updatedDebtor)
		{
			var jsonData = CreateJsonContent(updatedDebtor);

			var response = _httpClient.PutAsync(new Uri(_baseUri, updatedDebtor.DebtorId.ToString()), jsonData).GetAwaiter().GetResult();

			return ProcessResponse<GetDebtorResponse>(response);
		}

		/// <summary>
		/// Deletes a debtor by id.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the deleted debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public async Task<ApiResponse<object>> DeleteAsync(int debtorId)
		{
			var response = await _httpClient.DeleteAsync(new Uri(_baseUri, debtorId.ToString()));

			return await ProcessResponseAsync<object>(response);
		}

		/// <summary>
		/// Deletes a debtor by id.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the deleted debtor</returns>
		/// <exception cref="InvalidOperationException">Thrown when the API call returned unexpected data.</exception>
		public ApiResponse<object> Delete(int debtorId)
		{
			var response = _httpClient.DeleteAsync(new Uri(_baseUri, debtorId.ToString())).GetAwaiter().GetResult();

			return ProcessResponse<object>(response);
		}

		/// <summary>
		/// Gets a mandate for a debtor by reference.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <param name="reference">The mandate reference.</param>
		/// <returns>an api response containing the mandate</returns>
		public async Task<ApiResponse<GetMandateResponse>> GetMandateByReferenceAsync(int debtorId, string reference)
		{
			var response = await _httpClient.GetAsync(new Uri(_baseUri, string.Format("{0}/mandates/{1}", debtorId, reference)));

			return await ProcessResponseAsync<GetMandateResponse>(response);
		}

		/// <summary>
		/// Gets a mandate for a debtor by reference.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <param name="reference">The mandate reference.</param>
		/// <returns>an api response containing the mandate</returns>
		public ApiResponse<GetMandateResponse> GetMandateByReference(int debtorId, string reference)
		{
			var response = _httpClient.GetAsync(new Uri(_baseUri, string.Format("{0}/mandates/{1}", debtorId, reference))).GetAwaiter().GetResult();

			return ProcessResponse<GetMandateResponse>(response);
		}

		/// <summary>
		/// Gets a list of mandate for a debtor.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>an api response containing the list of mandates</returns>
		public async Task<ApiResponse<IList<GetMandateResponse>>> GetMandatesAsync(int debtorId)
		{
			var response = await _httpClient.GetAsync(new Uri(_baseUri, string.Format("{0}/mandates", debtorId)));

			return ProcessResponseAsync<IList<GetMandateResponse>>(response).GetAwaiter().GetResult();
		}

		/// <summary>
		/// Gets a list of mandate for a debtor.
		/// </summary>
		/// <param name="debtorId"></param>
		/// <returns></returns>
		public ApiResponse<IList<GetMandateResponse>> GetMandates(int debtorId)
		{
			var response = _httpClient.GetAsync(new Uri(_baseUri, string.Format("{0}/mandates", debtorId))).GetAwaiter().GetResult();

			return ProcessResponse<IList<GetMandateResponse>>(response);
		}

		/// <summary>
		/// Creates a new mandate.
		/// </summary>
		/// <param name="newMandate">The new mandate.</param>
		/// <returns>an api response containing the newly created mandate</returns>
		public async Task<ApiResponse<GetMandateResponse>> CreateMandateForDebtorAsync(CreateMandateRequest newMandate)
		{
			var jsonData = CreateJsonContent(newMandate);

			var response = await _httpClient.PostAsync(new Uri(_baseUri, string.Format("{0}/mandates", newMandate.DebtorId)), jsonData);

			return await ProcessResponseAsync<GetMandateResponse>(response);
		}

		/// <summary>
		/// Creates a new mandate.
		/// </summary>
		/// <param name="newMandate">The new mandate.</param>
		/// <returns>an api response containing the newly created mandate</returns>
		public ApiResponse<GetMandateResponse> CreateMandateForDebtor(CreateMandateRequest newMandate)
		{
			var jsonData = CreateJsonContent(newMandate);

			var response = _httpClient.PostAsync(new Uri(_baseUri, string.Format("{0}/mandates", newMandate.DebtorId)), jsonData).GetAwaiter().GetResult();

			return ProcessResponse<GetMandateResponse>(response);
		}

		/// <summary>
		/// Gets a list of invoices for a specific debtor asynchronously.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>An apiresponse containing the list of invoices.</returns>
		public async Task<ApiResponse<IList<GetInvoiceResponse>>> GetInvoicesAsync(int debtorId)
		{
			var response = await _httpClient.GetAsync(new Uri(_baseUri, string.Format("{0}/invoices", debtorId)));

			return ProcessResponse<IList<GetInvoiceResponse>>(response);
		}

		/// <summary>
		/// Gets a list of invoices for a specific debtor.
		/// </summary>
		/// <param name="debtorId">The debtor id.</param>
		/// <returns>An apiresponse containing the list of invoices.</returns>
		public ApiResponse<IList<GetInvoiceResponse>> GetInvoices(int debtorId)
		{
			var response = _httpClient.GetAsync(new Uri(_baseUri, string.Format("{0}/invoices", debtorId))).GetAwaiter().GetResult();

			return ProcessResponse<IList<GetInvoiceResponse>>(response);
		}

		/// <inheritdoc />
		public async Task<ApiResponse<BalanceResponse>> GetBalanceAsync(int debtorId, int year)
		{
			var response = await _httpClient.GetAsync(new Uri(_baseUri, $"{debtorId}/balance/{year}")).ConfigureAwait(false);

			return await ProcessResponseAsync<BalanceResponse>(response).ConfigureAwait(false);
		}
	}
}